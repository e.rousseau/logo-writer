import random

from constants import COLORS
from logoscripts import Context

rand = random.Random()

def new_global_context(logo):
    global_context = Context("global", logo=logo)

    global_context.append_sdk_prototype("av", forward)
    global_context.append_sdk_prototype("bc", turtle_write_on)
    global_context.append_sdk_prototype("cap", lambda c: c.logo.current_turtle.angle)
    global_context.append_sdk_prototype("dr", turn_right)
    global_context.append_sdk_prototype("entier", lambda c, num: int(num))
    global_context.append_sdk_prototype("fcap", set_turtle_angle)
    global_context.append_sdk_prototype("fpos", set_turtle_pos)
    global_context.append_sdk_prototype("fcouleur", color)
    global_context.append_sdk_prototype("ga", turn_left)
    global_context.append_sdk_prototype("hasard", lambda c: rand.random())
    global_context.append_sdk_prototype("lc", turtle_write_off)
    global_context.append_sdk_prototype("montre", logo_print)
    global_context.append_sdk_prototype("nommepage", namepage)
    global_context.append_sdk_prototype("pos", get_turtle_pos)
    global_context.append_sdk_prototype("ramene", loadpage)
    global_context.append_sdk_prototype("re", backward)
    global_context.append_sdk_prototype("sauvepage", lambda c: c.logo.save())
    global_context.append_sdk_prototype("vg", reset_graphics)
    global_context.append_sdk_prototype("vp", reset_page)

    return global_context


def namepage(context, pagename):
    context.logo.pagename = pagename


def loadpage(context, pagename):
    context.logo.load(pagename)


def set_turtle_angle(context, angle):
    context.logo.current_turtle.angle = angle


def reset_page(context):
    set_turtle_pos(context, 0, 0)
    set_turtle_angle(context, 90)
    reset_graphics(context)


def reset_graphics(context):
    context.logo.clear_canvas()


def logo_print(context, text):
    context.logo.console().print(text)
    return None


def turn_right(context, amount):
    context.logo.current_turtle.turn_left(-amount)
    return None


def turn_left(context, amount):
    context.logo.current_turtle.turn_left(amount)
    return None


def forward(context, amount):
    context.logo.current_turtle.forward(amount)
    return None


def backward(context, amount):
    context.logo.current_turtle.forward(-amount)
    return None


def color(context, color_index):
    color_index = int(color_index) % len(COLORS)
    context.logo.current_turtle.colors = COLORS[color_index]


def turtle_write_on(context):
    context.logo.current_turtle.set_pen_drawing(True)


def turtle_write_off(context):
    context.logo.current_turtle.set_pen_drawing(False)


def set_turtle_pos(context, x, y):
    context.logo.current_turtle.set_pos(x, y)


def get_turtle_pos(context):
    t = context.logo.current_turtle
    return [t.x, t.y]

