# A Python implementation of Logo writer

This was my first Python program beside the traditionnal *helloword* or *fizz buzz*. I used
it to learn Python in a rush, to ramp up on a job.

## Logo Writer

Logo Writer was a MS-DOS implementation of the well known LOGO programming language 
for kids. I learnt computer programming with it when I was 13 years old. 
This is very nostalgic.

Logo Writer had some features that was pretty advanced for a program written in 1988. 
It establised several concepts that are still prevalent in an another programming environment
for kids: *Scratch*. 

 * LogoWR allowed to save your programs on disk.
 * It was possible to write procedures (functions) 
 * Customizing the turtle (a sprite) to whatever you wanted. 
 * It even had matrix operations and array indexing... that was quite advanced subjects
   for a 13 years old kid. 

This python implementation is nearly complete. The LOGO language 
interpreter has some bugs unfortunately, it's mainly because I used 
`YACC` as lexer and parser. `YACC` it's quite rigid and debugging it is a nightmare plus, 
it lacks some ways to declare priorities when the grammar has conflicting rules. I should
have used `ANTLR`

The code is not very good though: 
 * It isn't well documented, the structure is quite non python-ish
 * It's built on Python 2 (the version we were using at the job back then)
 * I wasn't aware of how to optimize Python code... this is quite apparent, 
   list comprehension isn't used at all, and probably a lot of code could be
   removed by building on top of some python libraries.

Nonethelessly, I'm proud of this little project. That was a very interresting
experiment.
