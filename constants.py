from pygame.constants import USEREVENT

DEFAULT_CHAR_MAP = ("abcdefghijklmnopqrstuvwxyz"
                    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                    "0123456789:()/.,!?\"'@- +*["
                    "]<>_=")

COLORS = [
    (0, 0, 0),
    (0, 0, 128),
    (0, 128, 0),
    (0, 128, 128),
    (128, 0, 0),
    (128, 0, 128),
    (128, 64, 0),
    (128, 128, 128),

    (127, 127, 127),
    (127, 127, 255),
    (127, 255, 127),
    (127, 255, 255),
    (255, 127, 127),
    (255, 127, 255),
    (255, 255, 0),
    (255, 255, 255),
]

COLOR_BLACK = COLORS[0]
COLOR_WHITE = COLORS[15]



EVENT_BLINK = USEREVENT + 1