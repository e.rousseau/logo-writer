import math
import traceback

import pygame

import logoscripts
import parse
from constants import COLOR_WHITE, COLORS, COLOR_BLACK, EVENT_BLINK

HUD_COLOR = 5

TURTLE_IMG = b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x01' \
             b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x01\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00' \
             b'\x00\x00\x00\x00\x01\x01\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x01\x00\x00' \
             b'\x00\x00\x00\x00\x00\x00\x00\x01\x01\x00\x00\x01\x01\x01\x01\x00\x00\x01\x01\x00\x00\x00\x00\x00\x01' \
             b'\x01\x01\x01\x01\x01\x01\x01\x01\x01\x00\x00\x00\x00\x00\x00\x00\x01\x01\x01\x01\x01\x01\x01\x01\x00' \
             b'\x00\x00\x00\x00\x00\x00\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x00\x00\x00\x00\x00\x00\x01\x01\x01' \
             b'\x01\x01\x01\x01\x01\x01\x01\x00\x00\x00\x00\x00\x00\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x00\x00' \
             b'\x00\x00\x00\x00\x00\x01\x01\x01\x01\x01\x01\x01\x01\x00\x00\x00\x00\x00\x00\x00\x01\x00\x01\x01\x01' \
             b'\x01\x01\x01\x00\x01\x00\x00\x00\x00\x00\x01\x01\x00\x00\x00\x01\x01\x00\x00\x00\x01\x01\x00\x00\x00' \
             b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' \
             b'\x00\x00\x00\x00\x00\x00'


class Turtle(object):
    TURTLE_IMG = []

    def __init__(self, logo):
        self._img = None
        self._shape = 0
        self.x = 0
        self.y = 0
        self._angle = 90
        self._logo = logo
        self._surface = None
        self._color = COLOR_WHITE
        self._size = (0, 0)
        self._pen_drawing = True

    @property
    def shape(self):
        if self._surface is None:
            inc = 360 / len(self.TURTLE_IMG)
            offset = inc / 2
            angle = (self._angle + offset - 90) % 360
            i = int(angle / inc)

            self._surface = self.TURTLE_IMG[i].copy()

            if self._color != COLOR_WHITE:
                self._surface = self._surface.copy()
                self._surface.set_palette_at(1, self._color)

            self._size = self._surface.get_size()

        return self._surface

    def set_pen_drawing(self, value):
        self._pen_drawing = value

    @property
    def size(self):
        return self._size

    @property
    def colors(self):
        return self._color

    @colors.setter
    def colors(self, c):
        self._color = c
        self._surface = None

    @property
    def pos(self):
        return [self.x, self.y]

    def set_pos(self, x, y):
        x1, y1 = self.x, self.y
        self.x, self.y = self._warp_coords(x, y)

        if not self._pen_drawing:
            return

        self._draw_line(x1, y1, x, y)

    def forward(self, amount):
        dx = math.cos((self._angle * math.pi) / 180)
        dy = math.sin((self._angle * math.pi) / 180)

        x2 = self.x + dx*amount
        y2 = self.y + -dy*amount

        self.set_pos(x2, y2)

    def turn_left(self, amount):
        self.angle = self._angle + amount

    @property
    def pos(self):
        return self.x, self.y

    @property
    def angle(self):
        return self._angle

    @angle.setter
    def angle(self, angle):
        self._angle = angle % 360
        if self._shape == 0:
            self._surface = None

    def _draw_line(self, x1, y1, x2, y2):
        hw = self._logo.canvas.get_width() / 2.0
        hh = self._logo.canvas.get_height() / 2.0
        x1, y1 = x1 + hw, y1 + hh
        x2, y2 = x2 + hw, y2 + hh

        x = x2 % self._logo.canvas.get_width()
        y = y2 % self._logo.canvas.get_height()

        pygame.draw.line(
            self._logo.canvas,
            self._color,
            (round(x1), round(y1)),
            (round(x2), round(y2))
        )

        # TODO : must be debugged this part isn't accurate
        if x - x2 or y - y2:
            pygame.draw.line(
                self._logo.canvas,
                self._color,
                (round(x), round(y)),
                (round(x - (x2-x1)), round(y - (y2-y1)))
            )

    def _warp_coords(self, x, y):
        w = self._logo.canvas.get_width()
        hw = w/2.0
        h = self._logo.canvas.get_height()
        hh = h/2.0
        return ((x + hw) % w) - hw, ((y + hh) % h) - hh


class TextInput(object):
    CTRL_CHARS = {u'\b', u'\x7f', u'\r'}

    def __init__(self, text_buffer, visible_lines=8, ypos=1):
        self.buffer = text_buffer
        self._ypos = ypos
        self._visible_lines = visible_lines

    def handle_event(self, logo, event):
        pass

    def update_screen(self, logo):
        pos = self._ypos
        font = logo.font
        y, x = self.buffer.cursor_pos
        for L in range(0, self._visible_lines):
            line_str = self.buffer.get_line(L)
            font.draw(logo.viewport, (0, pos), line_str, )

            if L == y:
                pygame.draw.line(
                    logo.viewport,
                    COLOR_WHITE,
                    (font.w * x, pos),
                    (font.w * x, pos + font.h),
                    2
                )
            pos += font.h


class CommandInput(TextInput):
    def __init__(self, text_buffer, visible_lines=8, ypos=1):
        super().__init__(text_buffer, visible_lines, ypos)
        self._command_context = None

    def print(self, text):
        self.buffer.insert_str(str(text))
        self.buffer.goto_end()
        self.buffer.carriage_rtn()

    def handle_event(self, logo, event):
        def clear_history():
            while len(self.buffer) > self._visible_lines:
                self.buffer.delete_line(0)

        if event.type == pygame.KEYDOWN:
            char = event.unicode

            # TODO : Investigate Windows vs Linux for this... weird.
            if event.key == pygame.K_DELETE:
                char = u'\x7f'

            if event.key == pygame.K_HOME:
                y, _ = self.buffer.cursor_pos
                self.buffer.cursor_pos = (y, 0)
                return True
            elif event.key == pygame.K_END:
                self.buffer.goto_end()
                return True
            elif event.key == pygame.K_LEFT:
                y, x = self.buffer.cursor_pos
                self.buffer.cursor_pos = (y, x - 1)
                return True
            elif event.key == pygame.K_RIGHT:
                y, x = self.buffer.cursor_pos
                self.buffer.cursor_pos = (y, x + 1)
                return True
            elif event.key == pygame.K_UP:
                y, x = self.buffer.cursor_pos
                self.buffer.cursor_pos = (y - 1, x)
                return True
            elif event.key == pygame.K_DOWN:
                y, x = self.buffer.cursor_pos
                self.buffer.cursor_pos = (y + 1, x)
                return True
            elif char in self.CTRL_CHARS:
                if char == '\r':
                    try:
                        self.buffer.goto_end()
                        ast = parse.input(self.buffer.get_line())

                        result = logoscripts.run(self.command_context(logo), ast)
                        if result is not None:
                            self.print(result)
                        else:
                            self.buffer.carriage_rtn()

                    except Exception as e:
                        self.print(e)
                        print(str(e))
                        traceback.print_exc()
                    finally:
                        self.buffer.goto_end()
                        clear_history()
                else:
                    self.buffer.insert(char)
                clear_history()
                return True

            if char == u'' or not logo.font.has_char(char):
                return False
            self.buffer.insert(char)
            clear_history()
            return True

    def command_context(self, logo):
        if not self._command_context:
            self._command_context = logoscripts.Context(
                name="Command",
                parent=logo.page_context
            )
        return self._command_context


class CodeInput(TextInput):
    def __init__(self, text_buffer, visible_lines=8, ypos=1):
        super().__init__(text_buffer, visible_lines, ypos)

    def handle_event(self, logo, event):
        if event.type == pygame.KEYDOWN:
            char = event.unicode

            # TODO : Investigate Windows vs Linux for this... weird.
            if event.key == pygame.K_DELETE:
                char = u'\x7f'

            if event.key == pygame.K_HOME:
                y, _ = self.buffer.cursor_pos
                self.buffer.cursor_pos = (y, 0)
                return True
            elif event.key == pygame.K_END:
                y, _ = self.buffer.cursor_pos
                self.buffer.cursor_pos = (y, len(self.buffer.get_line(y)))
                return True
            elif event.key == pygame.K_LEFT:
                y, x = self.buffer.cursor_pos
                self.buffer.cursor_pos = (y, x - 1)
                return True
            elif event.key == pygame.K_RIGHT:
                y, x = self.buffer.cursor_pos
                self.buffer.cursor_pos = (y, x + 1)
                return True
            elif event.key == pygame.K_UP:
                y, x = self.buffer.cursor_pos
                self.buffer.cursor_pos = (y - 1, x)
                return True
            elif event.key == pygame.K_DOWN:
                y, x = self.buffer.cursor_pos
                self.buffer.cursor_pos = (y + 1, x)
                return True
            elif char in self.CTRL_CHARS:
                self.buffer.insert(char)
                return True

            if char == u'' or not logo.font.has_char(char):
                return False
            self.buffer.insert(char)
            return True


class Screen(object):
    def __init__(self, viewport_size, scale):
        self.viewport_size = (
            viewport_size[0] * scale[0],
            viewport_size[1] * scale[1],
        )
        self.screen = None
        self.viewport = None
        self.scaling = scale

    def show(self):
        pygame.init()
        pygame.key.set_repeat(200, 38)
        self.screen = pygame.display.set_mode(self.viewport_size)

        self.viewport = pygame.Surface((
            int(self.viewport_size[0]/self.scaling[0]),
            int(self.viewport_size[1]/self.scaling[1]),
        ))

    def update_screen(self):
        pygame.transform.scale(
            self.viewport,
            (int(self.viewport_size[0]), int(self.viewport_size[1])),
            self.screen
        )

        pygame.display.flip()

    def clear(self):
        self.viewport.fill(COLOR_BLACK)


def build_rotations(surface, increments=8):
    images = []
    size = surface.get_size()

    for i in range(0, 360, increments):
        dest = pygame.Surface(size, 0, 8)
        dest.set_palette(surface.get_palette())
        dest.set_colorkey((0, 0, 0, 0))

        tmp = pygame.transform.rotate(surface, i)
        size2 = tmp.get_size()
        dest.blit(tmp, ((size[0]-size2[0])/2, (size[1]-size2[1])/2))
        images.append(dest)

    return images


class Blinker(object):
    def __init__(self):
        self._is_on = True

    def __bool__(self):
        return self._is_on

    def reset(self):
        self._is_on = True

    def handle_event(self, event):
        if event.type == EVENT_BLINK:
            self._is_on = not self._is_on
            return True
        return False


class Mouse(object):
    """Mouse handles mouse events and convert them to a cursor state"""
    def __init__(self, offset=(0, 0), area=None):
        self._buttons = {}
        self._pos = (0, 0)
        # todo remove these two attributes, doesn't belong here
        self._offset = offset
        self._area = None
        self.set_area(area)

    def set_area(self, area):
        if area is None:
            self._area = None
            return

        self._area = pygame.Rect(area)

    def handle_event(self, event):
        result = False
        if event.type == pygame.MOUSEMOTION:
            result = True
        elif event.type == pygame.MOUSEBUTTONDOWN:
            self._buttons[event.button] = True
            result = True
        elif event.type == pygame.MOUSEBUTTONUP:
            self._buttons[event.button] = False
            result = True

        if result:
            self._pos = event.pos

        return result

    def inside_region(self):
        return (not self._area) or self._area.collidepoint(self._pos)

    def reset(self):
        self._buttons = {}

    def button(self, index):
        if index not in self._buttons:
            return False

        return self._buttons[index]

    @property
    def pos(self):
        return self._pos[0] + self._offset[0], self._pos[1] + self._offset[1]


class Window(object):
    """this object is responsible for rendering the base frame, aka title and
    mid bar etc..."""

    STATUS_BAR = pygame.Rect(0, 160, 320, 8)

    def __init__(self, screen, font):
        self._header_bar = "Chargement..."
        self._font = font
        self._screen = screen

    def update_header_bar(self, title, mode):
        self._header_bar = '{s:{c}^{n}}'.format(s=title, n=40, c='-')
        if mode:
            self._header_bar = self._header_bar[:-len(mode)] + mode

    def update_screen(self):
        """must be called to update screen"""
        self._font.draw(self._screen.viewport, (0, 0), self._header_bar, HUD_COLOR)

        pygame.draw.rect(self._screen.viewport, COLORS[HUD_COLOR],
                         self.STATUS_BAR)