import pygame

from constants import COLORS


class Font:
    def __init__(self, img, glyphs, size):
        self.glyphs = glyphs
        self.w, self.h = size
        self.img = img

    def draw(self, surface, pos, text, color=15):
        x, y = pos
        self.img.set_palette_at(2, COLORS[color])
        for c in text:
            i = ord(c)
            surface.blit(self.img, (x, y), self.glyphs[i])
            x += self.w

    def draw_on_bg(self, surface, pos, text, color=0, bg_color=15):
        x, y = pos
        h, w = self.h, self.w * len(text)
        pygame.draw.rect(surface, COLORS[bg_color], pygame.Rect(x, y, w, h))

        self.img.set_palette_at(2, COLORS[color])
        for c in text:
            i = ord(c)
            surface.blit(self.img, (x, y), self.glyphs[i])
            x += self.w

    def draw_l(self, surface, pos, text):
        x, y = pos

        for c in reversed(text):
            i = ord(c)
            x -= self.w
            surface.blit(self.img, (x, y), self.glyphs[i])

    def has_char(self, c):
        i = ord(c)
        if i < 0 or i > 255:
            return False
        return self.glyphs[i] is not None

def load_font(path, charmap, size):
    """
    load_font from a file path. The size(x,y) is used to extract all chars
    """
    img = pygame.image.load(path)

    w, h = img.get_size()
    cw, ch = size

    if w % cw != 0:
        return None, "surface not of proper width compared to provided size"
    if h % ch != 0:
        return None, "surface not of proper height compared to provided size"

    glyphs = []

    for y in range(int(h / ch)):
        for x in range(int(w / cw)):
            g = pygame.Rect(x * cw, y * ch, cw, ch)
            glyphs.append(g)

    font = [None] * 256

    for pos, c in enumerate(charmap):
        i = ord(c)
        if i >= 256:
            continue

        font[i] = glyphs[pos]

    return Font(img, font, size), None