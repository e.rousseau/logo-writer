import ast
from inspect import signature

from parse import FuncCall, FuncDef


class ScriptError(Exception):
    def __init__(self, *args: object, **kwargs: object) -> None:
        super().__init__(*args, **kwargs)


class Prototype(object):
    def __init__(self, name, var_count, func, var_names=None):
        self.name = name
        self.var_count = var_count
        self.func = func
        self.var_names = var_names or []
        self.reserved = False

    def run(self, context, *args, **kwargs):
        return self.func(context, *args, **kwargs)


class Context(object):
    def __init__(self, name, logo=None, parent=None):
        if not logo and not parent:
            raise ValueError("'logo' and 'parent' cannot be unset")

        if logo and parent:
            raise ValueError("'logo' and 'parent' are mutually exclusive")

        if parent:
            logo = parent.logo

        self.name = name
        self.variables = dict()
        self.prototypes = dict()
        self.stack = []
        self.logo = logo
        self._parent = parent

    def subcontext(self, stack):
        context = Context(self.logo, parent=self)
        context.stack = stack
        return context

    def var_value(self, name):
        if name in self.variables:
            return self.variables[name]

        if self._parent is not None:
            return self._parent.var_value(name)

        raise ScriptError("Ne sait que faire pour :{}".format(name))

    def prototype(self, name):
        if name in self.prototypes:
            return self.prototypes[name]

        if self._parent is not None:
            return self._parent.prototype(name)

        raise ScriptError("Ne sait que faire pour {}".format(name))

    def append_sdk_prototype(self, name, func):
        args = signature(func)
        p = Prototype(name, len(args.parameters)-1, func)
        p.reserved = True  # lock it
        self.prototypes[name] = p

    def append_user_prototype(self, func_def):
        name = func_def.id
        # Check if the method is reserved somewhere
        is_reserved = False
        try:
            is_reserved = self.prototype(name).reserved
        except ScriptError:
            pass
            # This is ok, doesn't exists

        if is_reserved:
            raise ScriptError("Nom de procedure '{} invalide".format(name))

        def run_user_func(context, *args, **kwargs):
            for var_names in kwargs:
                context.variables[var_names] = kwargs[var_names]
            return run(context, func_def.program)

        var_names = []
        if func_def.prototype:
            var_names = func_def.prototype.body

        p = Prototype(
            name,
            len(var_names),
            run_user_func,
            var_names
        )

        self.prototypes[name] = p

    def clear(self):
        """clear this context from any definitions"""
        self.variables = dict()
        self.prototypes = dict()


def while_loop(context, node):
    i = 0
    last = None
    while i < run(context, node.test):
        last = run(context, node.body)
        i = i + 1
    return last


def branch(context, node):
    test = run(context, node.test)
    if test:
        return run(context, node.body)
    elif node.orelse is not None:
        return run(context, node.orelse)

    return test


def func_call(context, node):
    prototype = context.prototype(node.id)

    if prototype.var_names:
        return prototype.run(
            context,
            **dict(zip(prototype.var_names, context.stack))
        )
    else:
        return prototype.run(context, *context.stack)


def func_def(context, node):
    context.append_user_prototype(node)
    return None


def assign_var(context, node):
    value = run(context, node.value)
    context.variables[node.targets] = value
    return value


def read_var(context, node):
    return context.var_value(node.value)


def program(context, node):
    last = None

    stmt_iter = iter(node.body)

    while True:
        try:
            stmt = next(stmt_iter)
        except StopIteration:
            break

        if isinstance(stmt, FuncCall):
            last = run_func(context, stmt, stmt_iter)
        else:
            last = run(context, stmt)

    return last


operations = {
    '+': lambda x, y: x+y,
    '-': lambda x, y: x-y,
    '/': lambda x, y: x/y,
    '*': lambda x, y: x*y,
    '>': lambda x, y: 1 if x > y else 0,
    '<': lambda x, y: 1 if x < y else 0,
    '=': lambda x, y: 1 if x == y else 0,
    '!=': lambda x, y: 1 if x != y else 0,
    '<=': lambda x, y: 1 if x <= y else 0,
    '>=': lambda x, y: 1 if x >= y else 0,
}


uoperations = {
    '-': lambda x: -x
}


def unary_op(context, node):
    operand = run(context, node.operand)
    return uoperations[node.op](operand)


def bin_op(context, node):
    left = run(context, node.left)
    right = run(context, node.right)
    return operations[node.op](left, right)


def num(context, node):
    return node.n


def string(context, node):
    return node.s


NODE_TO_FUNC = {
    ast.UnaryOp: unary_op,
    ast.Num: num,
    FuncCall: func_call,
    FuncDef: func_def,
    ast.While: while_loop,
    ast.If: branch,
    ast.Assign: assign_var,
    ast.BinOp: bin_op,
    ast.Expression: program,
    ast.NameConstant: read_var,
    ast.Str: string,
}


def run(context, node):
    if node is None:
        return None

    return NODE_TO_FUNC[type(node)](context, node)


def run_func(context, func_node, stmt_iter):
    stack_func = context.prototype(func_node.id)
    arg_stack = []

    while True:
        if len(arg_stack) == stack_func.var_count:
            break

        try:
            arg = next(stmt_iter)
        except StopIteration:
            # We get there if the statement list is shorter than required params
            raise ScriptError("{} manque des parametres".format(stack_func.name))

        arg_stack.append(run(context, arg))

    # Call the function
    #context.stack = arg_stack
    return run(context.subcontext(arg_stack), func_node)
