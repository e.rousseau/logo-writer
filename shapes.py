import pickle

import pygame

from constants import COLOR_WHITE, COLORS
from display import Window, HUD_COLOR, Blinker, Mouse


class Shapes(object):
    """Shapes holds actual shape data, shapes are images for sprite commonly
    refered as characters on old consoles. """
    DIMX, DIMY = 16, 16
    SIZE = DIMX * DIMY

    def __init__(self, count=64, surface=None):
        self._count = count

        if surface:
            if surface.get_width() != Shapes.DIMX:
                raise ValueError("File isn't proper width")
            self._count = int(surface.get_height() / self.DIMY)
            self._ram = surface
            return

        self._ram = pygame.Surface((self.DIMX, self.DIMY*count), 0, 8)

        pygame.draw.line(self._ram, COLOR_WHITE, (0,0), (15,15))
        pygame.draw.line(self._ram, COLOR_WHITE, (15,0), (0,15))

    def get_pixels(self, index, target_surface=None):
        # todo bounds check
        result = target_surface
        if target_surface is None:
            result = self.create_empty_pixels()

        result.blit(self._ram, (0, -index*self.DIMY))
        return result

    def set_pixels(self, index, pixels):
        # todo bounds check
        self._ram.blit(pixels, (0, index*self.DIMY))

    def __len__(self):
        return self._count

    def create_empty_pixels(self):
        return pygame.Surface((self.DIMX, self.DIMY), 0, 8)

    def save(self, filename="formes.shp"):
        data = {
            "x": self._ram.get_width(),
            "y": self._ram.get_height(),
            "shp": pygame.image.tostring(self._ram, "P"),
            "pal": self._ram.get_palette(),
        }
        with open(filename, "wb") as file:
            pickle.dump(data, file)

    @classmethod
    def load(cls, filename="formes.shp"):
        try:
            with open(filename, "rb") as file:
                data = pickle.load(file)

                img = pygame.image.fromstring(
                    data["shp"],
                    (data["x"], data["y"]),
                    "P",
                )

                pal = data["pal"]
                img.set_palette(pal)
                return Shapes(surface=img)
        except Exception as e:
            # If something goes wrong above, we generate a new one.
            return Shapes()


class PageState(object):
    """Abstraction for different states in a document"""
    def __init__(self, view):
        self._view = view

    def update_screen(self):
        raise NotImplemented()

    def handle_event(self, event):
        raise NotImplemented()


class ShapeEditor(PageState):
    """ShapeEditor take a single shape and allow editing it."""
    GRID_COLOR = (60, 60, 60, 255)

    def __init__(self, view, shape_surface):
        super().__init__(view)
        self._blinker = Blinker()
        self._shape_surface = shape_surface
        self._w = shape_surface.get_width()
        self._h = shape_surface.get_height()
        self._scale = 8
        self.surface = pygame.Surface(
            (self._w*self._scale+1, self._h*self._scale+1),
            0, 8
        )

        self._shape_change_callbacks = []
        self._xcur, self._ycur = 0, 0
        self._draw_color = 255

        self._mouse = Mouse(
            offset=(-ShapeView.MAX_X, -8),
            area=(ShapeView.MAX_X, 8, self.surface.get_width(), self.surface.get_height()))

    def on_shape_change(self, callback):
        self._shape_change_callbacks.append(callback)

    def handle_event(self, event):
        result = False
        if self._mouse.handle_event(event) and self._mouse.button(1) and self._mouse.inside_region():
            x, y = self._mouse.pos
            nx, ny = int(x / self._scale), int(y / self._scale)
            if event.type == pygame.MOUSEMOTION:
                if self._xcur == nx and self._ycur == ny:
                    return

            if event.type == pygame.MOUSEBUTTONDOWN:
                color = self._shape_surface.get_at_mapped((nx, ny))
                self._draw_color = 0 if color else 255

            self._xcur = nx
            self._ycur = ny
            self.set_pixel((self._xcur, self._ycur), self._draw_color)
            result = True

        if self._blinker.handle_event(event):
            result = True
        elif event.type == pygame.KEYDOWN:
            self._blinker.reset()
            if event.key == pygame.K_LEFT:
                self._xcur = (self._xcur - 1) % self._w
                result = True
            elif event.key == pygame.K_RIGHT:
                self._xcur = (self._xcur + 1) % self._w
                result = True
            elif event.key == pygame.K_UP:
                self._ycur = (self._ycur - 1) % self._h
                result = True
            elif event.key == pygame.K_DOWN:
                self._ycur = (self._ycur + 1) % self._h
                result = True
            elif event.key == pygame.K_SPACE:
                self.swap_pixel((self._xcur, self._ycur))
                result = True

        if result:
            self.update_screen()

        return result

    def set_pixel(self, pos, color):
        self._shape_surface.set_at(pos, color)
        self._notice_shape_change_listeners()

    def swap_pixel(self, pos):
        x, y = pos
        color = self._shape_surface.get_at_mapped((x, y))
        color = 0 if color else 255

        self.set_pixel((x, y), color)

    def _notice_shape_change_listeners(self):
        for c in self._shape_change_callbacks:
            c(self._shape_surface)

    def set_edited_shape(self, shape_surface):
        self._shape_surface = shape_surface
        self.update_screen()

    def update_screen(self):
        for x in range(0, self._w):
            for y in range(0, self._h):
                c = self._shape_surface.get_at((x, y))

                self.surface.fill(
                    c,
                    pygame.Rect(
                        x*self._scale,
                        y*self._scale,
                        self._scale,
                        self._scale,
                    ),
                )

        for x in range(0, self._w+1):
            xx = x * self._scale
            pygame.draw.line(
                self.surface,
                self.GRID_COLOR,
                (xx, 0),
                (xx, self.surface.get_height())
            )
        for y in range(0, self._h+1):
            yy = y * self._scale
            pygame.draw.line(
                self.surface,
                self.GRID_COLOR,
                (0, yy),
                (self.surface.get_width(), yy)
            )

        if self._blinker:
            pygame.draw.rect(
                self.surface,
                COLOR_WHITE,
                (
                    self._xcur*self._scale,
                    self._ycur*self._scale,
                    self._scale+1,
                    self._scale+1,
                ),
            )


class Navigate(PageState):
    """Navigate through every shapes in a single Shapes object """
    def __init__(self, view, viewport, shapes):
        super().__init__(view)
        self._shape_index = 0
        self._shapes = shapes
        self._blinker = Blinker()
        self._viewport = viewport
        self._cursor_dims = None
        self._callbacks = []

    def update_screen(self):
        if not self._cursor_dims:
            self._update_cursor()

        if self._blinker:
            self._viewport.fill(COLORS[HUD_COLOR], self._cursor_dims)

    def on_shape_selection_change(self, callback):
        self._callbacks.append(callback)

    def handle_event(self, event):
        result = False
        if self._blinker.handle_event(event):
            result = True
        elif event.type == pygame.KEYDOWN:
            row_size = int(self._view.MAX_X / self._shapes.DIMX)

            if event.key == pygame.K_LEFT:
                self._select(self._shape_index - 1)
                result = True
            elif event.key == pygame.K_RIGHT:
                self._select(self._shape_index + 1)
                result = True
            elif event.key == pygame.K_UP:
                self._select(self._shape_index - row_size)
                result = True
            elif event.key == pygame.K_DOWN:
                self._select(self._shape_index + row_size)
                result = True

            if result:
                self._blinker.reset()
                self._update_cursor()
        return result

    def index(self):
        return self._shape_index

    def _update_cursor(self):
        row_size = self._view.MAX_X / self._shapes.DIMX
        w, h = self._shapes.DIMX, self._shapes.DIMY
        x = (self._shape_index % row_size) * w
        y = 8 + int(self._shape_index / row_size) * h
        self._cursor_dims = pygame.Rect(x-1, y-1, w+1, h+2)

    def _select(self, index):
        old = self._shape_index
        index = max(0, index)
        index = min(len(self._shapes)-1, index)
        self._shape_index = index

        if old != index:
            for c in self._callbacks:
                c(index)


class ShapeView(object):
    """ShapeView all the shapes. This is the glue between the different views"""
    MAX_X = 160

    def __init__(self, app, screen, shapes, font):
        self._app = app
        self._shapes = shapes
        self._screen = screen
        self._font = font
        self._window = Window(screen, font)
        self._title = None
        self.title = "FORMES"

        self._shape_editor = None
        self._navigate = Navigate(self, screen.viewport, shapes)
        self._navigate.on_shape_selection_change(self._update_shape_editor)
        self._page_state = self._navigate

        self._blit_surface = None

    def update_screen(self):
        """must be called to update screen"""
        self._screen.clear()
        self._window.update_screen()
        self._navigate.update_screen()
        self._redraw_shapes()

        pygame.draw.line(
            self._screen.viewport,
            COLORS[HUD_COLOR],
            (self.MAX_X, 8),
            (self.MAX_X, 160),
        )

        if self._shape_editor:
            self._screen.viewport.blit(
                self._shape_editor.surface,
                (self.MAX_X+1, 8),
            )

            w, h = self._shape_editor.surface.get_size()

            self._font.draw(
                self._screen.viewport,
                (self.MAX_X + w/2, h + 16),
                str(self._navigate.index() + 1))

        self._screen.update_screen()

    def handle_event(self, event):
        if self._shape_editor is None:
            self._update_shape_editor(0)

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                if self._page_state is self._navigate:
                    self._page_state = self._shape_editor
                else:
                    self._page_state = self._navigate
                return True
            elif event.key == pygame.K_ESCAPE:
                self._shapes.save()
                self._app.quit_page()
                return True

        return self._page_state.handle_event(event)

    def _edited_shape_changed(self, shape_surface):
        self._shapes.set_pixels(self._navigate.index(), shape_surface)

    def _update_shape_editor(self, shape):
        pixels = self._shapes.get_pixels(shape)
        if self._shape_editor is None:
            self._shape_editor = ShapeEditor(self, pixels)
            self._shape_editor.on_shape_change(self._edited_shape_changed)
            self._shape_editor.update_screen()
        else:
            self._shape_editor.set_edited_shape(pixels)

    def _redraw_shapes(self):
        sh = self._shapes
        SW, SH = sh.DIMX, sh.DIMY
        if self._blit_surface is None:
            self._blit_surface = pygame.Surface((SW, SH), 0, 8)
            self._blit_surface.set_colorkey(0)

        x, y = 0, 8
        shape_index = 0
        while shape_index < len(sh):
            if y > self._window.STATUS_BAR.y:
                break
            if x > self.MAX_X - SW:
                x = 0
                y += SH

            sh.get_pixels(shape_index, self._blit_surface)
            self._screen.viewport.blit(self._blit_surface, (x, y))
            x += SW
            shape_index = shape_index + 1

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self, title):
        self._title = title
        self._window.update_header_bar(self._title, "")
