import ast

import ply.lex as lex
import ply.yacc as yacc

reserved = {
    "donne": "ASSIGN",
    "repete": "LOOP",
    "pour": "FUNC",
    "fin" : "END",
    "si": "IF",
    "sisinon": "IFELSE",
}

tokens = [
             # Identifiers
             'ID',
             'NUMBER_LITERAL',
             'WORD',
             'VARIABLE',

             # Arithmetics
             'BOOL_CMP',
             'ADD_OP',
             'MUL_OP',
             'LBRACE',
             'RBRACE',
             'LSQBRACE',
             'RSQBRACE',
         ] + list(reserved.values())


t_BOOL_CMP = r'>|<|=|<=|>=|!='
t_ADD_OP = r'[+-]'
t_MUL_OP = r'[*/]'
t_LBRACE = r'\('
t_RBRACE = r'\)'
t_LSQBRACE = r'\['
t_RSQBRACE = r'\]'
t_ignore = ' \t'

precedence = (
    ("left", "WORD"),
    ("left", "VARIABLE"),
    ("left", "ID"),
)


def t_VARIABLE(t):
    r""":[a-zA-Z_][a-zA-Z0-9_]*"""
    t.value = t.value[1:]
    return t

def t_WORD(t):
    r"""
    "[a-zA-Z0-9_ ]*"|"[a-zA-Z0-9_]*
    """
    v = t.value[1:]
    if v[-1] == '"':
        v = v[:-1]

    t.value = v
    return t


def t_ID(t):
    r"""[a-zA-Z_][a-zA-Z0-9_]*"""
    t.type = reserved.get(t.value, 'ID')
    return t


def t_NUMBER_LITERAL(t):
    r"""\d+(\.\d+)?"""
    if "." in t.value:
        t.value = float(t.value)
    else:
        t.value = int(t.value)

    return t


def t_newline(t):
    r"\n+"
    t.lexer.lineno += len(t.value)


def t_error(t):
    print("Ne sais que faire pour '{}'".format(t.value[0]))
    t.lexer.skip(1)


class FuncCall(ast.AST):
    # no doc
    def __init__(self, id):  # real signature unknown
        self.id = id


class FuncDef(ast.AST):
    def __init__(self, id, prototype, program):
        self.id = id
        self.prototype = prototype
        self.program = program


def p_program(p):
    """program : program stmt
               | program assign
               | program funcdef
               | program subprogram
               | stmt
               | assign
               | empty

    """
    if len(p) == 3:
        p[1].body.append(p[2])
        p[0] = p[1]
    else:
        p[0] = ast.Expression([p[1]])



def p_braced_program(p):
    """subprogram : LSQBRACE program RSQBRACE"""
    p[0] = p[2]


def p_empty(p):
    """empty : """
    pass


def p_statement(p):
    """stmt : expr
            | ctrl
    """
    p[0] = p[1]


def p_string_statement(p):
    """stmt : WORD"""
    p[0] = ast.Str(p[1])


def p_statement_func_call(p):
    """stmt : ID"""
    p[0] = FuncCall(p[1])


def p_assign(p):
    """assign : ASSIGN WORD subprogram
              | ASSIGN WORD stmt
    """
    p[0] = ast.Assign(p[2], p[3])


def p_expression_op(p):
    """expr : expr ADD_OP term
    """
    p[0] = ast.BinOp(p[1], p[2], p[3])


def p_ctrl(p):
    """ctrl : loop
            | if
            | ifelse
    """
    p[0] = p[1]


def p_function(p):
    """funcdef : FUNC ID funcproto program END"""
    id = p[2]
    proto = p[3]
    program = p[4]
    p[0] = FuncDef(id, proto, program)


def p_func_prototype(p):
    """funcproto : VARIABLE funcproto
                 | empty
    """
    if len(p) == 3:
        if p[2] is None:
            p[0] = ast.Expression([])
            p[0].body.append(p[1])
        else:
            p[2].body.append(p[1])
            p[0] = p[2]


def p_ctrl_loop(p):
    """loop : LOOP expr subprogram"""
    p[0] = ast.While(p[2], p[3], None)


def p_ctrl_if(p):
    """if : IF expr subprogram"""
    p[0] = ast.If(p[2], p[3], None)

def p_ctrl_ifelse(p):
    """ifelse : IFELSE expr subprogram subprogram"""
    p[0] = ast.If(p[2], p[3], p[4])


def p_expression_term(p):
    """expr : term
    """
    p[0] = p[1]


def p_term_op(p):
    """term : term MUL_OP factor"""
    p[0] = ast.BinOp(p[1], p[2], p[3])


def p_term_factor(p):
    """term : factor"""
    p[0] = p[1]


def p_factor_braced(p):
    """factor : LBRACE expr RBRACE"""
    p[0] = p[2]


def p_term_neg_factor(p):
    """factor : ADD_OP factor"""
    if p[1] == '-':
        p[0] = ast.UnaryOp(p[1], p[2])
    else:
        p[0] = p[2]

def p_factor_litteral(p):
    """factor : NUMBER_LITERAL"""
    p[0] = ast.Num(p[1])


def p_factor_variable(p):
    """factor : VARIABLE"""
    p[0] = ast.NameConstant(p[1])


def p_factor_comparison(p):
    """factor : comparison"""
    p[0] = p[1]


def p_comparison(p):
    """comparison : factor BOOL_CMP factor"""
    p[0] = ast.BinOp(p[1], p[2], p[3])


def p_error(p):
    print("Ne sais que faire pour '{}'".format(p.value[0]))

lex.lex()
parser = yacc.yacc(outputdir="generated")


def input(data):
    result = yacc.parse(data)
    return result
