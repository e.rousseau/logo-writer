import traceback

import pygame

import ldk
import logoscripts
import parse
import pickle
from constants import COLOR_BLACK, COLORS, DEFAULT_CHAR_MAP
from display import Turtle, CommandInput, TURTLE_IMG, \
    build_rotations, CodeInput, Window
from logoscripts import Context
from text import load_font


class Logo(object):
    """
    Logo is the main class to interract with. Maybe we should extract the
    data part to a model.
    """

    def __init__(self, app, screen, font):
        self._app = app
        self.screen = screen
        self._window = Window(screen, font)
        self.font = font
        self._turtle = Turtle(self)
        self.global_context = ldk.new_global_context(self)
        self.page_context = Context("page", parent=self.global_context)
        self.canvas = pygame.Surface((320, 152))
        self._canvas_pos = (0, 8)

        self._command_input = CommandInput(TextBuffer(), 4, 168)
        self._page_program = CodeInput(TextBuffer(), 20, 8)

        self._canvas_state = CanvaState(self, self._command_input)
        self._program_state = ProgramState(self, self._page_program)
        self._page_state = self._canvas_state

        self._pagename = None
        self.title = "???"

    @property
    def pagename(self):
        """pagename is the filename of the document"""
        return self._pagename

    @pagename.setter
    def pagename(self, name):
        if not name:
            return

        self._pagename = name
        self.title = name

    def flip_page(self):
        """flip_page puts the document in the edit program mode"""
        if self._page_state is self._canvas_state:
            self._page_state = self._program_state
        else:
            self._page_state = self._canvas_state
            self.reload_page_program()

        self._window.update_header_bar(self._title, self._page_state.name())

    def reload_page_program(self):
        """reload the program behind the actual page"""
        program = str(self._page_program.buffer)
        self.page_context.clear()
        try:
            # TODO : we must ensure only function are accepted
            ast = parse.input(program)
            logoscripts.run(self.page_context, ast)
        except Exception as e:
            self._command_input.print(e)
            print(str(e))
            traceback.print_exc()

    def update_screen(self):
        """must be called to update screen"""
        self.screen.clear()
        #self.font.draw(self.screen.viewport, (0, 0), self._header_bar, HUD_COLOR)
        self._window.update_screen()

        self._page_state.update_screen()
        self.screen.update_screen()

    def handle_event(self, event):
        return self._page_state.handle_event(event)

    def clear_canvas(self):
        pygame.draw.rect(
            self.canvas,
            COLOR_BLACK,
            pygame.Rect(
                0, 0,
                self.canvas.get_width(), self.canvas.get_height()
            )
        )

    def console(self):
        """the zone where commands are entered"""
        return self._command_input

    @property
    def current_turtle(self):
        return self._turtle

    @property
    def viewport(self):
        return self.screen.viewport

    def _load_assets(self):
        self.font, _ = load_font("./data/font_white.png", DEFAULT_CHAR_MAP, (8, 8))
        turtle = pygame.image.fromstring(TURTLE_IMG, (16, 16), "P")
        turtle.set_palette_at(1, COLORS[15])
        Turtle.TURTLE_IMG = build_rotations(turtle, increments=5)

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self, title):
        self._title = title
        self._window.update_header_bar(self._title, self._page_state.name())

    def save(self):
        if not self._pagename:
            raise ValueError("Votre page n'est pas nommee")

        with open("{}.lwr".format(self._pagename), "wb") as file:
            pickle.dump(str(self._page_program.buffer), file)

    def load(self, pagename):
        if not pagename:
            raise ValueError("Nom page non valide")

        if not pagename.lower().endswith(".lwr"):
            pagename = "{}.lwr".format(pagename)

        with open(pagename, "rb") as file:
            program = pickle.load(file)
            self._page_program.buffer.set_text(program)
            self.pagename = pagename[:-4]
            self.reload_page_program()


class PageState(object):
    """Abstraction for different states in a document"""
    def __init__(self, logo):
        self._logo = logo

    def update_screen(self):
        raise NotImplemented()

    def handle_event(self, event):
        raise NotImplemented()

    def name(self):
        return ""


class CanvaState(PageState):
    """CanvaState is the mode where the Turtles are drawn"""
    def __init__(self, logo, command_input):
        super().__init__(logo)
        self._command_input = command_input

    def update_screen(self):
        # Draw canvas
        self._logo.screen.viewport.blit(self._logo.canvas, self._logo._canvas_pos)

        # Turtle blitting
        canvas_pos = self._logo._canvas_pos
        turtle = self._logo.current_turtle
        pos = (canvas_pos[0] + turtle.pos[0] - turtle.size[0] / 2 + self._logo.canvas.get_width() / 2,
               canvas_pos[1] + turtle.pos[1] - turtle.size[1] / 2 + self._logo.canvas.get_height() / 2,
               )
        self._logo.screen.viewport.blit(turtle.shape, pos)

        # Draw command at bottom
        self._command_input.update_screen(self._logo)

    def handle_event(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_t and event.mod & pygame.KMOD_LCTRL:
                self._logo.flip_page()
                return
            elif event.key == pygame.K_ESCAPE:
                self._logo._app.quit_page()

            return self._command_input.handle_event(
                self._logo,
                event
            )
        return False


class ProgramState(PageState):
    """ProgramState is the mode where the user is editing the backpage
    program"""
    def __init__(self, logo, program_input):
        super().__init__(logo)
        self._program_input = program_input

    def update_screen(self):
        self._program_input.update_screen(self._logo)

    def handle_event(self, event):
        if event.type == pygame.KEYDOWN:

            if event.key == pygame.K_t and event.mod & pygame.KMOD_LCTRL:
                self._logo.flip_page()
                return

            return self._program_input.handle_event(
                self._logo,
                event
            )
        return False

    def name(self):
        return "verso"


class TextBuffer(object):
    """TextBuffer is used as TextView model. A renderer can use to display
    cursor and stuff. All manipulations texts are done using this Buffer."""
    def __init__(self, data=""):
        self._lines = data.split('\n')
        self._cursor_pos = (0, 0)
        self._mem_curpos = 0
        self.join_line_on_delete = True

    def set_text(self, data):
        self._lines = data.split('\n')
        self._cursor_pos = (0, 0)

    @property
    def cursor_pos(self):
        return self._cursor_pos

    @cursor_pos.setter
    def cursor_pos(self, coords):
        line, pos = coords
        is_line_move = False
        if pos == self._cursor_pos[1] and line != self._cursor_pos[0]:
            is_line_move = True

        line = max(0, min(len(self._lines)-1, line))
        line_str = self._lines[line]
        if is_line_move:
            pos = self._mem_curpos
            pos = max(0, min(len(line_str), pos))
        else:
            self._mem_curpos = pos
            pos = max(0, min(len(line_str), pos))

        self._cursor_pos = (line, pos)

    def get_line(self, p=None):
        if p is None:
            p, _ = self._cursor_pos

        if p < 0 or p >= len(self._lines):
            return ""
        return self._lines[p]

    def insert_str(self, string):
        self.insert(u'\r')
        line, pos = self._cursor_pos
        self._lines[line] = string

    def insert(self, char):
        if char == u'\b':
            self.backspace()
        elif char == u'\x7f':
            self.delete()
        elif char == u'\r':
            self.carriage_rtn()
        else:
            line, pos = self._cursor_pos
            line_str = self._lines[line]
            self._lines[line] = line_str[:pos] + char + line_str[pos:]
            self.cursor_pos = (line, pos+1)

    def backspace(self):
        line, pos = self._cursor_pos
        line_str = self._lines[line]
        if pos <= 0:
            if line <= 0 or not self.join_line_on_delete:
                return
            self.cursor_pos = (line-1, len(self._lines[line-1]))
            self._join_next_line(line-1)
            return

        self._lines[line] = line_str[:pos-1] + line_str[pos:]
        self.cursor_pos = (line, pos-1)

    def delete(self):
        line, pos = self._cursor_pos
        line_str = self._lines[line]
        if pos >= len(line_str):
            if not self.join_line_on_delete:
                return
            self._join_next_line(line)
            return
        self._lines[line] = line_str[:pos] + line_str[pos+1:]
        self.cursor_pos = (line, pos)

    def delete_line(self, line):
        cur_line, pos = self._cursor_pos
        if cur_line >= line:
            self.cursor_pos = (cur_line-1, pos)
        self._lines = self._lines[:line] + self._lines[line + 1:]

    def carriage_rtn(self):
        line, pos = self._cursor_pos
        line_str = self._lines[line]
        self._lines[line] = line_str[pos:]
        self._lines.insert(line, line_str[:pos])
        self.cursor_pos = (line+1, 0)

    def goto_end(self):
        y, _ = self.cursor_pos
        self.cursor_pos = (y, len(self.get_line(y)))

    def _join_next_line(self, line):
        if 0 <= line < len(self._lines)-1:
            line_str = self._lines[line+1]
            self._lines[line] = self._lines[line] + line_str
            self.delete_line(line+1)

    def __len__(self):
        return len(self._lines)

    def __str__(self):
        return '\n'.join(self._lines)
