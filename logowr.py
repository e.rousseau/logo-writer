import sys

import pygame

from browser import Browser
from constants import DEFAULT_CHAR_MAP, COLORS, EVENT_BLINK
from display import Screen, TURTLE_IMG, Turtle, build_rotations
from document import Logo
from shapes import Shapes, ShapeView
from text import load_font


class App(object):
    """App controls the workflow of the application"""

    MOUSE_EVT = [
        pygame.MOUSEMOTION,
        pygame.MOUSEBUTTONDOWN,
        pygame.MOUSEBUTTONUP,
    ]

    def __init__(self):
        self._app_quit = False
        self._page_to_load = None
        self._view = None
        self._page_quit = False
        self._show_shapes = False

    def load_page(self, filename):
        self._page_to_load = filename

    def new_page(self):
        self.load_page("")

    def show_shapes(self):
        self._show_shapes = True

    def quit_page(self):
        self._page_quit = True

    def main_loop(self):
        pygame.time.set_timer(EVENT_BLINK, 1000)

        while not self._app_quit:
            if self._view is None:
                self._view = Browser(self, screen, font)

            events = pygame.event.get()

            for e in events:
                if e.type in self.MOUSE_EVT:
                    e.pos = (
                        int(e.pos[0] / screen.scaling[0]),
                        int(e.pos[1] / screen.scaling[1]),
                    )

                if e.type == pygame.QUIT:
                    self._app_quit = True

                self._view.handle_event(e)
            self._view.update_screen()

            # process workflow
            if self._page_quit:
                self._view = None
                self._page_quit = False
            elif self._page_to_load is not None:
                self._view = Logo(self, screen, font)
                if self._page_to_load != "":
                    self._view.load(self._page_to_load)
                self._page_to_load = None
            elif self._show_shapes:
                self._view = ShapeView(self, screen, Shapes.load(), font)
                self._show_shapes = False


def _load_assets():
    font, _ = load_font("./data/font_white.png", DEFAULT_CHAR_MAP, (8, 8))
    turtle = pygame.image.fromstring(TURTLE_IMG, (16, 16), "P")
    turtle.set_palette_at(1, COLORS[15])
    Turtle.TURTLE_IMG = build_rotations(turtle, increments=5)
    return font


if __name__ == "__main__":
    sys.setrecursionlimit(10000)
    screen = Screen((320, 200), (3, 3))
    screen.show()
    font = _load_assets()
    App().main_loop()



