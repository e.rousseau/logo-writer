from os import walk

import pygame

from display import Window


class Browser(object):
    """Browser allows to select what to do when not in a document"""

    MENU_POS = (0, 32)

    def __init__(self, app, screen, font):
        self._app = app
        self._options = []
        self._window = Window(screen, font)
        self._window.update_header_bar("contenu", "")
        self._screen = screen
        self._font = font
        self._pos = 0
        self._file_listed = False

    def update_screen(self):
        if not self._file_listed:
            self._update_files()

        self._screen.clear()

        self._window.update_screen()

        x, y = self.MENU_POS

        for p, opt in enumerate(self._options):
            title, _ = opt
            if p == self._pos:
                self._font.draw_on_bg(
                    self._screen.viewport,
                    (x, y),
                    title,
                    0, 15
                )
            else:
                self._font.draw(
                    self._screen.viewport,
                    (x, y),
                    title,
                    15
                )

            y = y + 8

        self._font.draw(self._screen.viewport, (0, 8), "Utilisez les fleches pour choisir une", 15)
        self._font.draw(self._screen.viewport, (0, 16), "page. Appuyez sur RETOUR pour entrer.", 15)
        self._screen.update_screen()

    def handle_event(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                self._move_cursor_up()
                return True
            elif event.key == pygame.K_DOWN:
                self._move_cursor_down()
                return True
            elif event.key == pygame.K_RETURN:
                _, func = self._get_cursor_item()
                if func:
                    func()

    def _get_cursor_item(self):
        return self._options[self._pos]

    def _move_cursor_up(self):
        n = len(self._options)
        self._pos = (self._pos - 1) % n

        # Skip empty lines
        if self._options[self._pos][0] == "":
            self._move_cursor_up()

    def _move_cursor_down(self):
        n = len(self._options)
        self._pos = (self._pos + 1) % n

        # Skip empty lines
        if self._options[self._pos][0] == "":
            self._move_cursor_down()

    def _update_files(self):
        self._options = [
            ("NOUVELLE PAGE", lambda: self._app.new_page()),
            ("FORMES", lambda: self._app.show_shapes()),   # TODO: implementation of shapefile editor
            ("", None),
        ]

        def page_loader(file):
            def load_page():
                self._app.load_page(file)
            return load_page

        files = [(file[:-4], page_loader(file)) for file in list_files()]
        self._options.extend(files)
        self._file_listed = True


def list_files(path="."):
    f = []
    for (dirpath, dirnames, filenames) in walk(path):
        f.extend(filenames)
        break
    return filter(lambda file: file.lower().endswith(".lwr"), f)
